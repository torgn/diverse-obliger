#version 120

varying vec2 TexCoord;
varying vec3 N;
varying vec4 VSPosition;

uniform vec4 SpecularColor;
uniform sampler2D Earth;

void main() {

  vec4 L = gl_LightSource[0].position - VSPosition; 
  float d = length(L);   // distance between the light's position and the fragment
  float attenuation_factor = 1.0 / ( gl_LightSource[0].constantAttenuation + 
	                             gl_LightSource[0].linearAttenuation*d + 
	                             gl_LightSource[0].quadraticAttenuation*d*d );

  vec4 AmbientDiffuseColor = texture2D(Earth, TexCoord);  // sampled texture color is used for both ambient and diffuse material color
  vec4 Color = AmbientDiffuseColor * gl_LightSource[0].ambient * attenuation_factor;  // ambient contribution
  int Shininess = int(SpecularColor.w * 256);      // specular exponent  

  

  vec4 l = normalize(L);
  vec3 n = normalize(N);            //normalized normal vector 
  vec4 v = normalize(-VSPosition);
  vec4 h = normalize(l + v);        // half vector
       
  float nDotL = dot(n, l.xyz);
  if(nDotL > 0.0) {
    Color += AmbientDiffuseColor * gl_LightSource[0].diffuse * attenuation_factor;  // diffuse contribution
    float nDotH = dot(n, h.xyz);
    Color += pow(max(nDotH, 0.0), Shininess) * vec4(SpecularColor.xyz, 1.0) * gl_LightSource[0].specular * attenuation_factor;  //specular contribution
  }
 
  gl_FragColor = Color;
}
