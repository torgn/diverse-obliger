#version 120

varying vec2 TexCoord;
varying vec3 N;
varying vec4 VSPosition;


void main() {
  
  TexCoord = gl_MultiTexCoord0.st;  
  N = gl_NormalMatrix * gl_Normal;
  VSPosition = gl_ModelViewMatrix * gl_Vertex;   // vertex position in view space

  gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}
