/* -*- mode: C++; tab-width:4; c-basic-offset: 4; indent-tabs-mode:nil -*-
 *
 * Time-stamp: <2005-07-13 12:01:05 dyken>
 *
 * (C) 2005 Christopher Dyken, <dyken@cma.uio.no>
 *
 * Distributed under the GNU GPL.
 */

#include "TriMesh.hpp"

#include <stdexcept>
#include <vector>
#include <glm/glm.hpp>

using std::string;
using std::vector;

namespace GfxUtil {

TriMesh* TriMesh::subdivideLoop() {
  vector<glm::vec3> points;
  vector<int> indices;

  //skip
  // Generate a new set of points and indices using the Loop subdivision scheme

  //Create a new point from each existing node with updated coordinates
  for(size_t j=0; j<m_nodes.size(); j++) {
    Node vtx = m_nodes[j];
    HalfEdge* he = vtx.getLeadingHalfEdge();

    glm::vec3 p_new, p_old;
    p_old = vtx.m_pos_;

    vector<glm::vec3> p;

    do {
      p.push_back(he->getDestinationNode()->m_pos_);
      if(he->getVtxRingNext() == NULL)
        break;
      he = he->getVtxRingNext();
    } while(he != vtx.m_he_);

    size_t n = p.size();
    float beta = 1.0f/n * (5.0f/8.0f - pow(3.0f + 2.0f*glm::cos(2.0f*M_PI/n),2)/64.0f);    
    if(vtx.isBoundary()) {
      p.push_back(he->getNext()->getDestinationNode()->m_pos_);
      p_new = 3.0f/4.0f*p_old + 1.0f/8.0f*(p[0] + p[n]);  
    }
    else {
      p_new = (1.0f - n*beta)*p_old;
      for(size_t i=0; i<n; i++)
        p_new += beta*p[i];
    }
    
    points.push_back(p_new);

    // Indexing
    m_nodes[j].m_ix_ = j;
  }

  //Compute edge "midpoints" 
  for(size_t i=0; i<m_halfedges.size(); i++) {
    HalfEdge* he = m_halfedges[i];
    if(he->m_ix_ == (size_t)-1) { // -1 => not yet indexed
      glm::vec3 midpoint;
      vector<glm::vec3> p;
    
      p.push_back(he->getSourceNode()->m_pos_);
      p.push_back(he->getDestinationNode()->m_pos_);

      if(!he->isBoundary()) {
        p.push_back(he->getNext()->getDestinationNode()->m_pos_);
        p.push_back(he->getTwin()->getNext()->getDestinationNode()->m_pos_);
      
        midpoint = 3.0f/8.0f*(p[0] + p[1]) + 1.0f/8.0f*(p[2] + p[3]); 
      }
      else
        midpoint = 0.5f*(p[0] + p[1]);

      points.push_back(midpoint);

      // Indexing
      he->m_ix_ = points.size() - 1;
      if(he->getTwin() != NULL)
        he->getTwin()->m_ix_ = he->m_ix_;
    }
  }

  //Create triangles
  for(size_t j=0; j<m_triangles.size(); j++) {
    Triangle* t = m_triangles[j];
    HalfEdge* he[3] = {t->m_he_, t->m_he_->getNext(), t->m_he_->getPrev()};

    for(size_t i=0; i<3; i++) {
      indices.push_back(t->getNode(i)->m_ix_);
      indices.push_back(he[i]->m_ix_);
      indices.push_back(he[(i+2)%3]->m_ix_);
    }
    for(size_t i=0; i<3; i++) {
      indices.push_back(he[i]->m_ix_);
    }
  }
  // unskip

  return new TriMesh(points, indices);
}

}  // GfxUtil
