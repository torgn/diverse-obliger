/* $Id: SimpleViewer.cpp, v1.1 2011/09/20$
 *
 * Author: Christopher Dyken, <dyken@cma.uio.no>
 * Reviewed by: Bartlomiej Siwek, <bartloms@ifi.uio.no>
 *
 * Distributed under the GNU GPL.
 */

#include "SimpleViewer.hpp"

#include <cmath>
#include <stdexcept>
#include <GL/glew.h>
#include <GL/freeglut.h>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace GfxUtil {
  
SimpleViewer::SimpleViewer() {
  setViewVolume(glm::vec3(0.0f, 0.0f, 0.0f), 1.74);
}

void SimpleViewer::setWindowSize(int w, int h) {
  m_window_width = w;
  m_window_height = h;
  m_window_aspect = (float)w / (float)h;
}

void SimpleViewer::setViewVolume(glm::vec3 origin, float radius) {
  // skip
  // set viewvolume related variables...
  m_viewvolume_origin = origin;
  m_viewvolume_radius = radius;
  // unskip
}

void SimpleViewer::rotationBegin(int x, int y) {
  // skip
  // handle beginning of the rotation...
  m_unit_mousepos_i = getNormalizedCoords(x, y);  // registers mouse position when mousebutton was pressed
  m_rotation_mousepos_i = getPointOnUnitSphere(m_unit_mousepos_i);  // the corresponding mouse position on the unit sphere
  m_rotation_orientation_i = m_camera_orientation;
  m_state = ROTATING;
	
  // unskip
}

void SimpleViewer::panBegin(int x, int y) {
  // skip
  // handle beginning of panning... (OPTIONAL)
  // unskip
}

void SimpleViewer::zoomBegin(int x, int y) {
  // skip
  // handle beginning of zoom... (OPTIONAL)
  // unskip
}

void SimpleViewer::resetState(int /*x*/, int /*y*/) {
  m_state = NONE;
}

void SimpleViewer::motion(int x, int y) {
  // skip
  // handle mouse motion while one of the mouse buttons is down...
	m_unit_mousepos_c = getNormalizedCoords(x, y);  // registers current mouse position
	m_rotation_mousepos_c = getPointOnUnitSphere(m_unit_mousepos_c);  //the corresponding mouse position on the unit sphere 
	
	glm::fquat t = getGreatCircleRotation(m_rotation_mousepos_i, m_rotation_mousepos_c);

	m_camera_orientation = t * m_rotation_orientation_i;  // setting new camera orientation as described in the exercise text.

  // unskip
}

glm::mat4x4 SimpleViewer::getProjectionMatrix() {
  glm::mat4x4 result;
  
  // skip
  // compute projection matrix...
	result = glm::frustum(-1.0f, 1.0f, -1.0f, 1.0f, 1.5f, 20.0f);
  // unskip
  
  return result;
}

glm::mat4x4 SimpleViewer::getModelViewMatrix() {
  glm::mat4x4 result;
  
  // skip 
  // compute modelview matrix (a rotation should be preceded by a translation of the camera)...
	result = glm::translate(glm::mat4x4(1.0), glm::vec3(0.0, 0.0, -11.25)); // moving the origin into the center of the view frustum
	result = result * glm::mat4_cast(m_camera_orientation);  
  // unskip
  
  return result;
}

void SimpleViewer::renderDebugGraphics() {
  // This is a debug method that uses the deprecated API
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(-m_window_aspect, m_window_aspect, -1.0f, 1.0f, -1.0f, 1.0f);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  switch(m_state) {
    case ROTATING:
      glLineWidth(3.0);

      glColor3f(0.4, 0.4, 0.4);
      glBegin(GL_LINE_LOOP);
      for(size_t i=0; i<50; i++)
        glVertex2f(glm::cos(2.0*M_PI*i/(float)50), glm::sin(2.0*M_PI*i/(float)50));
      glEnd();

      glBegin(GL_LINES);
      glColor3f(1.0, 0.5, 0.5);
      glVertex3f(0.0f, 0.0f, 0.0f);
      glVertex3fv(&m_rotation_axis[0]);

      glColor3f(0.5, 1.0, 0.5);
      glVertex3f(0.0f, 0.0f, 0.0f);
      glVertex3fv(&m_rotation_mousepos_i[0]);

      glColor3f(0.5, 0.5, 1.0);
      glVertex3f(0.0f, 0.0f, 0.0f);
      glVertex3fv(&m_rotation_mousepos_c[0]);

      glEnd();
      break;
    default:
      // Do nothing.
      break;
  }
}

glm::vec2 SimpleViewer::getNormalizedCoords(int x, int y) {
  glm::vec2 result;
  // skip
  // compute the position of mouse in a coordinate system convinet for us...
	int w = m_window_width;  // for better readability
	int h = m_window_height; // ...	
	result = glm::vec2(x/float(w) - 0.5f, 0.5f - y/float(h));
  // unskip

  return result;
}

glm::vec3 SimpleViewer::getPointOnUnitSphere(glm::vec2 p) {
  glm::vec3 result;
  // skip
  // project mouse position in a convinient coordinate system to a unit sphere...
	float x = p[0], y = p[1];
	float r = sqrt(pow(x, 2) + pow(y, 2));

	if (r < 0.5f) {
	  result = glm::vec3(2*x, 2*y, sqrt(1 - 4*pow(r,2)));
	}
	else {
	  result = glm::vec3(x/r, y/r, 0.0f);
	}
  // unskip

  return result;
}

// This function computes the quaternion corresponding to the great circle rotation between two vectors
glm::fquat SimpleViewer::getGreatCircleRotation(glm::vec3 a, glm::vec3 b) {
  glm::vec3 axis = glm::cross(a, b);
  float length = glm::length(axis);
  if(length > std::numeric_limits<float>::epsilon()) {
    axis = axis / length;

    float angle = glm::acos(glm::dot(a,b));
    float c = glm::cos(0.5*angle);
    float s = glm::sin(0.5*angle);
    return glm::fquat(c, s*axis[0], s*axis[1], s*axis[2]);
  } else {
    return glm::fquat(1.0f, 0.0f, 0.0f, 0.0f);
  }
}

// This function computes the quaternion corresponding to the rotation by a give angle around given axis
glm::fquat SimpleViewer::getAxisAngleRotation(glm::vec3& axis, float angle) {
  float c = glm::cos(0.5f * angle);
  float s = glm::sin(0.5f * angle);
  return glm::fquat(c, s*axis[0], s*axis[1], s*axis[2]);
}

} // namespace GfxUtil
