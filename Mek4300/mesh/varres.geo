// Gmsh project created on Sat Feb 15 20:48:35 2014
Point(1) = {0, 0, 0, 0.005};
Point(2) = {0, -0.2, 0, 0.005};
Point(3) = {1, -0.2, 0, 0.1};
Point(4) = {1, 1, 0, 0.1};
Point(5) = {-1, 1, 0, 0.1};
Point(6) = {-1, 0, 0, 0.1};
Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {4, 5};
Line(5) = {5, 6};
Line(6) = {6, 1};
Line Loop(7) = {1, 2, 3, 4, 5, 6};
Plane Surface(8) = {7};
