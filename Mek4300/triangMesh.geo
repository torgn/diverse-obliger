// Gmsh project created on Thu Feb 13 16:16:31 2014
lc = 1.0/128.0;

Point(1) = {0, 0, 0, 1.0};
Point(2) = {0.5, 0.8660254037, 0, 1.0};
Point(3) = {-0.5, 0.8660254037, 0, 1.0};
Line(1) = {1, 3};
Line(2) = {3, 2};
Line(3) = {2, 1};
Line Loop(4) = {1, 2, 3};
Plane Surface(5) = {4};
