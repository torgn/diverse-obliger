#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <errno.h>
#include <mpi.h>

#define ERROR(message)                          \
  do {                                          \
    perror(message);				\
    MPI_Finalize();				\
    exit(EXIT_FAILURE);				\
  } while (0)


void allocate_matrix (double*** matrix, int num_rows, int num_cols)
{
  int i;
  *matrix = (double**)malloc(num_rows*sizeof(double*));
  (*matrix)[0] = (double*)malloc(num_rows*num_cols*sizeof(double));
  for (i=1; i<num_rows; i++)
    (*matrix)[i] = (*matrix)[i-1]+num_cols;
}

void deallocate_matrix (double*** matrix)
{
  free((*matrix)[0]);
  free(*matrix);
}

void  compute_matrix_product (double*** matrix_A, double*** matrix_B, int m, int l, int n, double*** result_matrix)
{
  int i,j,k;
#pragma omp parallel for collapse(2) private(k)
  for (i=0; i<m; i++) {
    for (j=0; j<n; j++) {
      for (k=0; k<l; k++) {
	(*result_matrix)[i][j] += (*matrix_A)[i][k] * (*matrix_B)[k][j];
      }
    }
  } 
}

void read_matrix_binaryformat (char* filename, double*** matrix, int* num_rows, int* num_cols)
{
  FILE* fp = fopen (filename,"rb");
  fread (num_rows, sizeof(int), 1, fp);
  fread (num_cols, sizeof(int), 1, fp);
  /* storage allocation of the matrix */
  allocate_matrix (matrix, *num_rows, *num_cols);
  /* read in the entire matrix */
  fread ((*matrix)[0], sizeof(double), (*num_rows)*(*num_cols), fp);
  fclose (fp);
}

void write_matrix_binaryformat (char* filename, double** matrix,
				int num_rows, int num_cols)
{
  FILE *fp = fopen (filename,"wb");
  fwrite (&num_rows, sizeof(int), 1, fp);
  fwrite (&num_cols, sizeof(int), 1, fp);
  fwrite (matrix[0], sizeof(double), num_rows*num_cols, fp);
  fclose (fp);
}

/* typedef struct */
/* { */
/*   double **M;  */
/*   int m;  // number of rows */
/*   int n;  // number of columns */
/* } */
/* matrix; */

int main(int argc, char *argv[])
{
  size_t i,j,k;
  double  **A, **B, **C, **A_sub, **B_sub, **C_sub;
  int m,l,n;
  int P, id;   // P = nr. of processes, id = process rank
  
  //initializing MPI
  MPI_INIT (&argc, &argv);
  MPI_Comm_rank (MPI_COMM_WORLD, &id);
  MPI_Comm_size (MPI_COMM_WORLD, &P);

  int p = (int) sqrt(P);
  // printf("p = %d\n", p);

  //testilng if P is square 
  if (pow(p,2) != P)
    ERROR("Number of processes not square\n");
  
  //reading input file names for matrix A and B and output file name for matrix C from the command line
  char *A_file, *B_file, *C_file;    
  /* if(argc < 3) { */
  /*   printf("argc = %d\n", argc); */
  /*   fprintf(stderr, "Usage: %s input_file_A input_file_B output_file_C\n", argv[0]); */
  /*   exit(EXIT_FAILURE); */
  /* } */
  
  errno = 0;
  A_file = argv[1];
  if (errno)
    ERROR("A_file");
  
  B_file = argv[2];
  if (errno)
    ERROR("B_file");
  
  C_file = argv[3];
  if (errno)
    ERROR("C_file");
  
  if (id == 0) {
    //reading A and B from the two input data files
    read_matrix_binaryformat (A_file, &A, &m, &l);
    read_matrix_binaryformat (B_file, &B, &l, &n);
    allocate_matrix (&C, m, n);
  }
  else {
    //to avoid segmentation faults with  MPI_Scatterv/MPI_Gatherv
    allocate_matrix (&A, 1, 1);
    allocate_matrix (&B, 1, 1);
    allocate_matrix (&C, 1, 1);
  }

  //broadcasting the dimensions of A and B
  MPI_Bcast (&m, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast (&l, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast (&n, 1, MPI_INT, 0, MPI_COMM_WORLD); 

  if ((m+l+n)%p != 0)
    ERROR("Dimensions of A and B not a multiple of the square root of number of processes\n");
  
  /* storage allocation of submatrices   */
  allocate_matrix (&A_sub, m/p, l/p);
  allocate_matrix (&B_sub, l/p, n/p);
  allocate_matrix (&C_sub, m/p, n/p);
  /* ... */

  //preparing arguments for scatterv/gatherv
  int sendcounts[P];
  for (i=0; i<P; i++) {
    sendcounts[i] = 1;
  }
  
  //displacement
  int displs_a[P];
  int displs_b[P];
  int displs_c[P];
  int counter = 0;
  for (i=0; i<p; i++) {
    for (j=0; j<p; j++) {
      displs_a[counter] = i*(m/p)*l + j*(l/p);
      displs_b[counter] = i*(l/p)*n + j*(n/p);
      displs_c[counter] = i*(m/p)*n + j*(n/p);
      counter+=1;
    }
  }
  
  /* creating a datatype for the submatrices of A, the submatrices of B and the submatrices of C to use with MPI_Scatterv/MPI_Gatherv */
  int sizes_A[2] = {m, l};
  int sizes_B[2] = {l, n};
  int sizes_C[2] = {m, n};
  int subsizes_A[2] = {(m/p), (l/p)};
  int subsizes_B[2] = {(l/p), (n/p)};
  int subsizes_C[2] = {(m/p), (n/p)};
  int starts[2] = {0,0};
  
  MPI_Datatype prototype, type_A, type_B, type_C;
  MPI_Type_create_subarray(2, sizes_A, subsizes_A, starts, MPI_ORDER_C, MPI_DOUBLE, &prototype);
  MPI_Type_create_resized(prototype, 0, sizeof(double), &type_A);
  MPI_Type_free(&prototype);
  
  MPI_Type_create_subarray(2, sizes_B, subsizes_B, starts, MPI_ORDER_C, MPI_DOUBLE, &prototype);
  MPI_Type_create_resized(prototype, 0, sizeof(double), &type_B);
  MPI_Type_free(&prototype);
  
  MPI_Type_create_subarray(2, sizes_C, subsizes_C, starts, MPI_ORDER_C, MPI_DOUBLE, &prototype);
  MPI_Type_create_resized(prototype, 0, sizeof(double), &type_C);
  MPI_Type_free(&prototype);
  
  MPI_Type_commit(&type_A);
  MPI_Type_commit(&type_B);
  MPI_Type_commit(&type_C);

   
  /* distributing the pieces of A and B */
  MPI_Scatterv(&A[0][0], sendcounts, displs_a, type_A, &A_sub[0][0], (m/p)*(l/p), MPI_DOUBLE, 0, MPI_COMM_WORLD);
  MPI_Scatterv(&B[0][0], sendcounts, displs_b, type_B, &B_sub[0][0], (l/p)*(n/p), MPI_DOUBLE, 0, MPI_COMM_WORLD);
  /* ... */

 
  /* creating communicator with cartesian topology */
  MPI_Comm comm_cart;
  int dims[2] = {p,p};
  int periods[2] = {1,1};
  MPI_Cart_create (MPI_COMM_WORLD, 2, dims, periods, 1, &comm_cart);
  /* ... */

  /* performing initial shift */
  int coords[2];                  //this process coordinates in the comm_cart communicator
  MPI_Cart_coords (comm_cart, id, 2, coords);

  int src_A, src_B;               //rank of source process relative to matrix A and B respectively
  int dest_A, dest_B;             //rank of destination process relative to A and B respectively

  MPI_Cart_shift (comm_cart, 1, -coords[0], &src_A, &dest_A);
  MPI_Cart_shift (comm_cart, 0, -coords[1], &src_B, &dest_B);

  MPI_Status status;
  MPI_Sendrecv_replace (&(A_sub[0][0]), m*l/P, MPI_DOUBLE, dest_A, 0, src_A, 0, comm_cart, &status);
  MPI_Sendrecv_replace (&(B_sub[0][0]), n*l/P, MPI_DOUBLE, dest_B, 1, src_B, 1, comm_cart, &status);  
  /* ... */
  
  /* initializing C_sub */
  for (i=0; i<m/p; i++)
    for (j=0; j<n/p; j++) 
      C_sub[i][j] = 0;
  /* ... */

  /* carrying out Cannon's algorithm */
  for (i=0; i<p; i++) {
    compute_matrix_product (&A_sub, &B_sub, m/p, l/p, n/p, &C_sub);
    MPI_Cart_shift (comm_cart, 1, -1, &src_A, &dest_A);
    MPI_Cart_shift (comm_cart, 0, -1, &src_B, &dest_B);
    MPI_Sendrecv_replace (&(A_sub[0][0]), m*l/P, MPI_DOUBLE, dest_A, 0, src_A, 0, comm_cart, &status);
    MPI_Sendrecv_replace (&(B_sub[0][0]), n*l/P, MPI_DOUBLE, dest_B, 1, src_B, 1, comm_cart, &status);
  }
  /* ... */

  /* gathering the pieces of C to process 0's C matrix*/
  MPI_Gatherv (&C_sub[0][0], m*n/P, MPI_DOUBLE, &C[0][0], sendcounts, displs_c, type_C, 0, MPI_COMM_WORLD);
  /* ... */

  /* writing matrix C to file */
  if (id==0)
    write_matrix_binaryformat (C_file, C, m, n);
  /* ... */
   
  deallocate_matrix (&A_sub);
  deallocate_matrix (&B_sub);
  deallocate_matrix (&C_sub);

  deallocate_matrix (&A);
  deallocate_matrix (&B);
  deallocate_matrix (&C);

  MPI_Finalize ();
}


