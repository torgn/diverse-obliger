#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <mpi.h>

#define ERROR(message)                          \
  do {                                          \
    perror(message);				\
    MPI_Finalize;				\
    exit(EXIT_FAILURE);				\
  } while (0)

// Block decomposition macros
#define BLOCK_LOW(id, p, n) ((id)*(n)/(p))   // determines the starting index of process id out of p's recieved block in an array of n elements
#define BLOCK_SIZE(id, p, n) (BLOCK_LOW((id)+1,p,n)-BLOCK_LOW((id),p,n))  // determines the number of elements in the block intended for process p


//make use of two functions from the simplejpeg library
void import_JPEG_file(const char *filename, unsigned char **image_chars,
		      int *image_height, int *image_width,
		      int *num_components);

void export_JPEG_file(const char *filename, unsigned char *image_chars,
		      int image_height, int image_width,
		      int num_components, int quality);

typedef struct
{
  float** image_data;  /* a 2D array of floats */
  int m;               /* # pixels in x-direction */
  int n;               /* # pixels in y-direction */
}
image;

void allocate_image(image *u, int m, int n)
{
  u->image_data = (float**) malloc(m*sizeof(float*));
  if(u->image_data == NULL)
    ERROR("malloc");

  u->image_data[0] = (float*) malloc(m*n*sizeof(float));
  if(u->image_data[0] == NULL) {
    free(u->image_data);
    ERROR("malloc");
  }

  size_t i;
  for(i=0; i<m; i++)
    u->image_data[i] = u->image_data[0] + n*i;

  u->m = m;
  u->n = n;
}

void deallocate_image(image *u)
{
  free(u->image_data[0]);
  free(u->image_data);
}

int main(int argc, char *argv[])
{
  size_t i,j,k;
  int m, n, c, iters;
  int my_m, my_n, my_rank, num_procs;
  float kappa;
  image u, u_bar;
  unsigned char *image_chars;
  char *input_jpeg_filename, *output_jpeg_filename;

  MPI_Init (&argc, &argv);
  MPI_Comm_rank (MPI_COMM_WORLD, &my_rank);
  MPI_Comm_size (MPI_COMM_WORLD, &num_procs);

  /* read from command line: kappa, iters, input_jpeg_filename, output_jpeg_filename */
  if(argc < 5){
    fprintf(stderr, "Usage: %s kappa iterations input_filename output_filename\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  errno = 0;
  kappa = strtof(argv[1], (char **) NULL);
  if(errno)
    ERROR("kappa");
  iters = atoi(argv[2]);
  if(errno)
    ERROR("iterations");
  input_jpeg_filename = argv[3];
  if(errno)
    ERROR("input_filename");
  output_jpeg_filename = argv[4];
  if(errno)
    ERROR("output_filename");
  /* ... */

  if(my_rank==0) {
    import_JPEG_file(input_jpeg_filename, &image_chars, &m, &n, &c);
    if(image_chars == NULL)
      ERROR("import_JPEG_file");
  }

  MPI_Bcast (&m, 1, MPI_INT, 0, MPI_COMM_WORLD);
  MPI_Bcast (&n, 1, MPI_INT, 0, MPI_COMM_WORLD);

  
  /* divide the m x n pixels evenly among the MPI processes */
  
  // only the interior rows are basis for the calculation, hence m-2. '+ 2' to impose overlap in the distributed data.
  my_m = BLOCK_SIZE(my_rank, num_procs, m-2) + 2;
  my_n = n;

  allocate_image (&u, my_m, my_n);
  allocate_image (&u_bar, my_m, my_n);

  /* each process asks process 0 for a partitioned region */
  /* of image_chars and copy the values into u */

  // preparing necessary arrays in order to use MPI_Scatterv
  int sendcnts[num_procs];
  int displs[num_procs];
  for(i=0;i<num_procs;i++) {
    sendcnts[i] = n*(BLOCK_SIZE(i, num_procs, m-2) + 2);
    displs[i] = n*(BLOCK_LOW(i, num_procs, m-2));
  }
  
  // prepering a recieve buffer
  if(my_rank)
    image_chars = (unsigned char*) malloc(my_m*my_n*sizeof(unsigned char));
  
  MPI_Scatterv(image_chars, sendcnts, displs,
	       MPI_UNSIGNED_CHAR, (my_rank)? image_chars : MPI_IN_PLACE, my_m*my_n,       
	       MPI_UNSIGNED_CHAR, 0, MPI_COMM_WORLD );

  //copying the partitioned region of image_chars into u
  for(i=0; i<my_m*my_n; i++)
    u.image_data[0][i] = (float) image_chars[i];
  /* ... */

  void iso_diffusion_denoising(image *u, image *u_bar, float kappa, int iters)
  {
    image *u_temp;
    int m = u->m;
    int n = u->n;
    
    //variables used in conjunction with communication between processes
    MPI_Status status;
    int prec = my_rank - 1;  // preceding process rank
    int succ = my_rank + 1;  // succeeding process rank

    /* copying vertical and horizontal boundary pixels of u into u_bar */
    // vertical boundary pixels:
    for(i=0; i<m; i++) {
      u_bar->image_data[i][0] = u->image_data[i][0];
      u_bar->image_data[i][u->n-1] = u->image_data[i][u->n-1];
    }

    // horizontal boundary pixels:
    if(my_rank==0) {
      for(j=1; j<n-1; j++)
    	u_bar->image_data[0][j] = u->image_data[0][j];

      prec = MPI_PROC_NULL;
    }
    else if(my_rank==num_procs-1) {
      for(j=1; j<n-1; j++)
    	u_bar->image_data[m-1][j] = u->image_data[m-1][j];
      
      succ = MPI_PROC_NULL;
    }
    /* ... */

    //starting iterations of iso_diffusion_denoising
    for(k=0; k<iters; k++) {
      for(i=1; i<m-1; i++) {
	for(j=1; j<n-1; j++) {
	  u_bar->image_data[i][j] = 
	    u->image_data[i][j] + kappa*( u->image_data[i-1][j]
					  + u->image_data[i][j-1]
					  - 4*u->image_data[i][j]
					  + u->image_data[i][j+1]
					  + u->image_data[i+1][j] );
	}
      }

      //Using MPI_Sendrecv to exchange data between neighboring processes after each iteration 
      MPI_Sendrecv(u_bar->image_data[m-2], n, MPI_FLOAT, succ, 1,
		   u_bar->image_data[0], n, MPI_FLOAT, prec, 1,
		   MPI_COMM_WORLD, &status);
      MPI_Sendrecv(u_bar->image_data[1], n, MPI_FLOAT, prec, 1,
		   u_bar->image_data[m-1], n, MPI_FLOAT, succ, 1,
		   MPI_COMM_WORLD, &status);

      //making the newly computed values of u_bar become the new values of u
      u_temp = u;
      u = u_bar;
      u_bar = u_temp;
    }
  }

  iso_diffusion_denoising (&u, &u_bar, kappa, iters);

  /* each process sends its resulting content of u_bar to process 0 */
  /* process 0 receives from each process incoming values and */
  /* copy them into the designated region of image_chars */

  // I've just done the same as above when distributing the data, only in reverse.

  for(i=0; i<my_m*my_n; i++)
    image_chars[i] = (unsigned char) u.image_data[0][i];
  
  //using the counterpart of MPI_Scatterv to send the denoised partitions of image_chars back into process 0's image_chars
  MPI_Gatherv((my_rank)? image_chars : MPI_IN_PLACE, my_m*my_n,
	      MPI_UNSIGNED_CHAR, image_chars, sendcnts, displs,
	      MPI_UNSIGNED_CHAR, 0, MPI_COMM_WORLD);
  /* ... */


  if(my_rank==0)
    export_JPEG_file(output_jpeg_filename, image_chars, m, n, c, 75);

  free(image_chars);

  deallocate_image (&u);
  deallocate_image (&u_bar);

  MPI_Finalize ();

  return 0;
}
